package com.creditrisk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.creditrisk.repository"})
@EntityScan(basePackages = {"com.creditrisk.model"})
public class CreditProposalApplication 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(CreditProposalApplication.class, args);
    }
}
