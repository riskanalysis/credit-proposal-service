package com.creditrisk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creditrisk.model.CreditProposal;
import com.creditrisk.service.CreditProposalService;

@RestController
@RequestMapping(path = "api/v1/creditproposal",produces = "application/json")
public class CreditProposalController {
	@Autowired
	private CreditProposalService service;
	
	@PostMapping("/createproposal")
	public ResponseEntity<CreditProposal> createProposal(@RequestBody CreditProposal proposal ) {
		return  ResponseEntity.ok(service.createCreditProposal(proposal));
	}
	
	@GetMapping("/getproposal/{id}")
	public ResponseEntity<CreditProposal> getProposalById(@PathVariable Long id ) {
		return  ResponseEntity.ok(service.getProposalById(id));
	}

	@PostMapping("/loadInitialData")
	public ResponseEntity<Integer> loadInitialData( ) {
		return  ResponseEntity.ok(service.loadInitialData());
	}
	
	@GetMapping("/getproposalByPageNo")
	public ResponseEntity<List<CreditProposal>> getProposalById() {
		return  ResponseEntity.ok(service.getListOFProposalOfGivenRange(10, 0));
	}
}
