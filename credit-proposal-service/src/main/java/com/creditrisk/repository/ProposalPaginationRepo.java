package com.creditrisk.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.creditrisk.model.CreditProposal;

@Repository
public interface ProposalPaginationRepo extends PagingAndSortingRepository<CreditProposal, Long>{

}
