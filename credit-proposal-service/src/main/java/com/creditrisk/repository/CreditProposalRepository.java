package com.creditrisk.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.creditrisk.model.CreditProposal;


public interface CreditProposalRepository extends JpaRepository<CreditProposal, Long> {
}
