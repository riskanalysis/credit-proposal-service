package com.creditrisk.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CreditProposal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long Id;
	public String assesmentYear;
	public String code;
	public Long frParam1;
	public Long frParam2;
	public Long frParam3;
	public Long brParam1;
	public Long brParam2;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getAssesmentYear() {
		return assesmentYear;
	}
	public void setAssesmentYear(String assesmentYear) {
		this.assesmentYear = assesmentYear;
	}
	public Long getFrParam1() {
		return frParam1;
	}
	public void setFrParam1(Long frParam1) {
		this.frParam1 = frParam1;
	}
	public Long getFrParam2() {
		return frParam2;
	}
	public void setFrParam2(Long frParam2) {
		this.frParam2 = frParam2;
	}
	public Long getFrParam3() {
		return frParam3;
	}
	public void setFrParam3(Long frParam3) {
		this.frParam3 = frParam3;
	}
	public Long getBrParam1() {
		return brParam1;
	}
	public void setBrParam1(Long brParam1) {
		this.brParam1 = brParam1;
	}
	public Long getBrParam2() {
		return brParam2;
	}
	public void setBrParam2(Long brParam2) {
		this.brParam2 = brParam2;
	}
	
	
}
