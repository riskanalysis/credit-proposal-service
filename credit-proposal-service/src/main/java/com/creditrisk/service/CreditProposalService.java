package com.creditrisk.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.creditrisk.model.CreditProposal;
import com.creditrisk.repository.CreditProposalRepository;
import com.creditrisk.repository.ProposalPaginationRepo;

@Service
public class CreditProposalService {
	@Autowired
	private CreditProposalRepository repository;

	@Autowired
	private ProposalPaginationRepo repo;

	public CreditProposal createCreditProposal(CreditProposal proposal) {
		return repository.save(proposal);
	}

	public CreditProposal getProposalById(Long id) {
		return repository.findById(id).orElse(null);
	}

	public int loadInitialData() {
		List<CreditProposal> proposals = new ArrayList<>();
		for (int i = 0; i < 200; i++) {
			CreditProposal proposal = new CreditProposal();
			proposal.setAssesmentYear("2020-2021");
			proposal.setCode("Proposal" + i);
			proposal.setBrParam1(Long.valueOf(3 + i));
			proposal.setBrParam2(Long.valueOf(2 + i));
			proposal.setFrParam3(Long.valueOf(4 + i));
			proposal.setFrParam1(Long.valueOf(5 + i));
			proposal.setFrParam2(Long.valueOf(6 + i));
			proposals.add(proposal);
		}
		List<CreditProposal> savedProposals = repository.saveAll(proposals);
		return savedProposals.size();
	}
	
	public List<CreditProposal> getListOFProposalOfGivenRange(int pageSize,int pageNo){
		Pageable paging = PageRequest.of(pageNo, pageSize);
		 
        Page<CreditProposal> pagedResult = repository.findAll(paging);
        return pagedResult.getContent();
	} 
}
